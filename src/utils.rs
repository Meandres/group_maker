use crate::structs::*;

pub fn debug_setup(setup: Setup) {
    println!("Setup :\n\tGroups:");
    println!("Average regret : {:?}", setup.average_regret);
    for i in 0..setup.groups.len() {
        println!("{:?}", setup.groups[i]);
    }
    println!("\nStudents :");
    for i in 0..setup.students.len() {
        println!("{:?}", setup.students[i]);
    }
}

fn print_group(group: Group, students: Vec<Student>) {
    println!("{}", group.name);
    for d in group.students.keys() {
        println!(
            "{} : {} - regret moyen : {:.2}",
            d,
            group.students.get(&(d.clone())).unwrap().iter().fold(
                String::from(""),
                |mut acc, s| {
                    let name = students.iter().find(|x| x.id == *s).unwrap().name.clone();
                    if acc.is_empty() {
                        acc.push_str(&name)
                    } else {
                        acc.push_str(&format!("; {}", name))
                    }
                    acc
                }
            ),
            group.average_regret.get(&(d.clone())).unwrap()
        );
    }
    println!();
}

fn print_student(days: Vec<String>, student: Student, groups: Vec<Group>) {
    println!("{}", student.name);
    for d in days {
        println!(
            "\t{} : {} - regret {}",
            d,
            groups
                .clone()
                .iter()
                .find(|x| x.id == student.assigned.get(&d).unwrap().0)
                .unwrap()
                .name
                .clone(),
            student.assigned.get(&d).unwrap().1
        );
    }
    println!();
}

pub fn output_setup(days: Vec<String>, setup: Setup) {
    println!("Résultats : \n\t");
    println!(
        "Regret moyen : {:.2}\n\nAteliers : \n",
        setup.average_regret
    );
    for i in 0..setup.groups.len() {
        print_group(setup.groups[i].clone(), setup.students.clone());
    }
    /*println!("\nEtudiants :\n");
    for i in 0..setup.students.len() {
        print_student(
            days.clone(),
            setup.students[i].clone(),
            setup.groups.clone(),
        );
    }*/
}

pub fn write_csv_files(days: Vec<String>, setup: Setup) {
    let mut w_groups = csv::Writer::from_path("groupes_constitues.csv")
        .expect("Error when opening the group file");
    let mut w_students = csv::Writer::from_path("assignations_etudiants.csv")
        .expect("Error when opening the student file");
    let groups = setup.groups;
    let students = setup.students;
    let mut row = Vec::new();
    row.push(String::from("Atelier"));
    for d in days.clone() {
        row.push(d);
    }
    w_groups
        .write_record(&row)
        .expect("Error when writing group header");
    for g in groups.clone() {
        row = Vec::new();
        row.push(g.name);
        for d in days.clone() {
            if g.students.get(&d).is_some() {
                row.push(g.students.get(&d).unwrap().iter().fold(
                    String::from(""),
                    |mut acc, s| {
                        let name = &students.iter().find(|x| x.id == *s).unwrap().name;
                        if acc.is_empty() {
                            acc.push_str(name)
                        } else {
                            acc.push_str(&format!("; {}", name))
                        }
                        acc
                    },
                ));
            } else {
                row.push(String::from(""));
            }
        }
        w_groups
            .write_record(&row)
            .expect("Error when writing a group");
    }
    row = Vec::new();
    row.push(String::from("Etudiant"));
    row.push(String::from("Adresse mail"));
    for d in days.clone() {
        row.push(d);
    }
    w_students
        .write_record(&row)
        .expect("Error when writing group header");
    for s in students {
        row = Vec::new();
        row.push(s.name);
        row.push(s.email);
        for d in days.clone() {
            row.push(
                groups
                    .clone()
                    .iter()
                    .find(|x| x.id == s.assigned.get(&d).unwrap().0)
                    .unwrap()
                    .name
                    .clone(),
            );
        }
        w_students
            .write_record(&row)
            .expect("Error when writing a group");
    }
}

pub fn print_usage() {
    println!("Usage : TODO");
}
