mod parser;
mod solver;
mod structs;
mod utils;

use crate::parser::*;
use crate::solver::*;
use crate::structs::*;
use crate::utils::*;
use std::collections::HashMap;
use std::env;
use std::io;

fn solve_all_days(
    setup: Setup,
    days: Vec<String>,
    bounds: Vec<HashMap<String, (u32, u32)>>,
) -> Setup {
    let mut working_setup: Setup = setup.clone();
    for i in 0..days.len() {
        let output = solve_group_assignement(working_setup, days[i].clone(), bounds[i].clone());
        if output.is_none() {
            panic!("Erreur lors de l'affectation le {}", days[i]);
        } else {
            working_setup = output.unwrap();
        }
    }
    let mut group_regret: Vec<HashMap<String, f64>> = Vec::new();
    for _ in 0..working_setup.groups.len() {
        group_regret.push(HashMap::new());
    }
    let mut students: Vec<Student> = Vec::new();
    let mut average_regret: f64 = 0.;
    working_setup
        .students
        .iter()
        .enumerate()
        .for_each(|(i, s)| {
            students.push(s.clone());
            students[i].preferences = setup.students[i].preferences.clone();
            average_regret += s.assigned.values().fold(0, |mut acc, a| {
                acc += a.1;
                acc
            }) as f64;
            for day in days.clone() {
                let assigned: (u32, u32) = *s.assigned.get(&day).unwrap();
                group_regret[assigned.0 as usize]
                    .entry(day)
                    .and_modify(|e| *e += assigned.1 as f64)
                    .or_insert(assigned.1 as f64);
            }
        });
    average_regret /= students.len() as f64;
    for i in 0..working_setup.groups.len() {
        for day in days.clone() {
            group_regret[working_setup.groups[i].id as usize]
                .entry(day.clone())
                .and_modify(|e| {
                    *e /= working_setup.groups[i].students.get(&day).unwrap().len() as f64
                });
        }
        working_setup.groups[i].average_regret =
            group_regret[working_setup.groups[i].id as usize].clone();
    }
    Setup {
        students,
        groups: working_setup.groups,
        average_regret,
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 {
        print_usage();
        return;
    }
    let csv_file = &args[1];
    let groups_file = &args[2];
    let mut setup = parse_csv(csv_file).unwrap();
    let (days, bounds) = parse_groups_file(groups_file);

    setup = solve_all_days(setup, days.clone(), bounds);
    output_setup(days.clone(), setup.clone());
    let mut answer = String::new();
    let stdin = io::stdin();
    let mut correct = false;
    while !correct {
        println!("\n\nVoulez-vous écrire ce résultat ? O/n");
        stdin
            .read_line(&mut answer)
            .expect("Error when reading user input");
        answer.truncate(1);
        if answer == "O" || answer == "n" {
            correct = true;
        }
    }
    if answer == "O" {
        write_csv_files(days, setup);
    }
}
