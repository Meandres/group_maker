use crate::structs::*;
use std::collections::HashMap;
use std::error::Error;

fn extract_letter(name: String) -> String {
    String::from(
        name.split_whitespace()
            .nth(1)
            .unwrap()
            .chars()
            .nth(0)
            .unwrap(),
    )
}

fn get_groups(mut vec_groups: Vec<&str>) -> Vec<Group> {
    vec_groups.sort();
    vec_groups
        .iter()
        .enumerate()
        .fold(Vec::new(), |mut v, (i, g)| {
            v.push(Group {
                id: i as u32,
                name: String::from((*g).trim()),
                short_name: extract_letter(String::from(*g)),
                students: HashMap::new(),
                average_regret: HashMap::new(),
            });
            v
        })
}

//supposes that every student has to order all the groups
pub fn parse_csv(path: &str) -> Result<Setup, Box<dyn Error>> {
    let mut groups: Vec<Group> = Vec::new();
    let mut students: Vec<Student> = Vec::new();
    let mut rdr = csv::ReaderBuilder::new()
        .delimiter(b';')
        .from_path(path)
        .unwrap();
    //let mut rdr = csv::Reader::from_path(path).unwrap();
    let mut i = 0;
    for result in rdr.records() {
        let record = result.unwrap();
        let vec_groups: Vec<&str> = record.get(4).unwrap().split(";").collect();
        if groups.is_empty() {
            groups = get_groups(vec_groups.clone());
        }
        let mut name = String::from(record.get(1).unwrap().trim());
        name.push(' ');
        name.push_str(record.get(2).unwrap().trim());
        students.push(Student {
            id: i,
            name: name,
            email: String::from(record.get(3).unwrap().trim()),
            preferences: vec_groups.iter().fold(Vec::new(), |mut v, g| {
                let ind: u32;
                match groups
                    .iter()
                    .position(|x| *x.name == String::from((*g).trim()))
                {
                    Some(i) => ind = i as u32,
                    None => {
                        groups.push(Group {
                            id: groups[groups.len() - 1].id + 1,
                            name: String::from(*g),
                            short_name: extract_letter(String::from(*g)),
                            students: HashMap::new(),
                            average_regret: HashMap::new(),
                        });
                        ind = (groups.len() - 1) as u32
                    }
                }
                v.push(ind as u32);
                v
            }),
            assigned: HashMap::new(),
        });
        i += 1;
    }
    Ok(Setup {
        students,
        groups,
        average_regret: 0.,
    })
}

pub fn parse_groups_file(path: &str) -> (Vec<String>, Vec<HashMap<String, (u32, u32)>>) {
    let mut days: Vec<String> = Vec::new();
    let mut bounds: Vec<HashMap<String, (u32, u32)>> = Vec::new();
    let mut rdr = csv::ReaderBuilder::new()
        .has_headers(false)
        .from_path(path)
        .expect("Error when reading the group file");
    let mut r = 0;
    for result in rdr.records() {
        let record = result.expect("Error when parsing the record");
        if r == 0 {
            for i in 1..record.len() {
                days.push(String::from(record.get(i).unwrap().trim()));
                bounds.push(HashMap::new());
            }
        } else {
            let mut tuple_bound;
            for i in 1..record.len() {
                let for_a_day = record.get(i).unwrap().trim();
                if for_a_day != "" {
                    let b: Vec<&str> = for_a_day.split('-').collect();
                    tuple_bound = (b[0].parse::<u32>().unwrap(), b[1].parse::<u32>().unwrap());
                    bounds[i - 1].insert(String::from(record.get(0).unwrap()), tuple_bound);
                }
            }
        }
        r += 1;
    }
    (days, bounds)
}
