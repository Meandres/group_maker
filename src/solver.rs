use crate::structs::*;
use good_lp::*;
use std::collections::HashMap;

fn regret_constraint(
    student: &Student,
    v_regrets: &Vec<Variable>,
    v_group_taken: &Vec<Variable>,
    nb_groups: usize,
) -> Constraint {
    let mut rhs = Expression::with_capacity(nb_groups);
    for i in 0..student.preferences.len() {
        rhs.add_mul(
            i as f64 + 1.,
            v_group_taken[student.id as usize * nb_groups + student.preferences[i] as usize],
        );
    }
    rhs.eq(v_regrets[student.id as usize])
}

fn assignement_constraint(
    student: &Student,
    v_group_taken: &Vec<Variable>,
    nb_groups: usize,
) -> Constraint {
    let mut rhs = Expression::with_capacity(nb_groups);
    for i in 0..nb_groups {
        rhs.add_mul(1, v_group_taken[student.id as usize * nb_groups + i]);
    }
    rhs.eq(Expression::from_other_affine(1))
}

fn group_constraint(
    group: &Group,
    v_group_taken: &Vec<Variable>,
    v_groups_fulliness: &Vec<Variable>,
    nb_students: usize,
) -> Constraint {
    let mut rhs = Expression::with_capacity(nb_students);
    for i in 0..nb_students {
        rhs.add_mul(
            1,
            v_group_taken[i * v_groups_fulliness.len() + group.id as usize],
        );
    }
    rhs.eq(v_groups_fulliness[group.id as usize])
}

pub fn solve_group_assignement(
    setup: Setup,
    day: String,
    bounds: HashMap<String, (u32, u32)>,
) -> Option<Setup> {
    let mut students = setup.students;
    let mut groups = setup.groups;
    let mut problem = ProblemVariables::new();
    println!(
        "Tailles : \n\tNb d'étudiants : {},\n\t Nb d'ateliers : {}",
        students.len(),
        groups.len()
    );
    let v_regrets = problem.add_vector(
        variable().integer().min(1).max(groups.len() as f64),
        students.len(),
    );
    let v_group_taken = problem.add_vector(
        variable().integer().min(0).max(1),
        students.len() * groups.len(),
    );
    let mut v_group_fulliness = Vec::new();
    for i in 0..groups.len() {
        let b: (u32, u32);
        if bounds.contains_key(&groups[i].short_name) {
            b = *bounds.get(&groups[i].short_name).unwrap();
        } else {
            b = *bounds.get(&String::from("Defaut")).unwrap();
        }
        v_group_fulliness.push(problem.add(variable().integer().min(b.0).max(b.1)));
    }
    let objective: Expression =
        v_regrets
            .iter()
            .fold(Expression::with_capacity(students.len()), |mut expr, r| {
                expr.add_mul(1, r);
                expr
            });
    let mut model = problem.minimise(objective.clone()).using(default_solver);
    for i in 0..students.len() {
        let c1 = regret_constraint(&students[i], &v_regrets, &v_group_taken, groups.len());
        model.add_constraint(c1);
        let c2 = assignement_constraint(&students[i], &v_group_taken, groups.len());
        model.add_constraint(c2);
    }
    for i in 0..groups.len() {
        model.add_constraint(group_constraint(
            &groups[i],
            &v_group_taken,
            &v_group_fulliness,
            students.len(),
        ));
    }
    let try_sol = model.solve();
    match try_sol {
        Err(a) => {
            println!("Error when solving the model : {}", a);
            return None;
        }
        Ok(sol) => {
            let mut preferences: Vec<Vec<u32>> = Vec::new();
            for i in 0..students.len() {
                preferences.push(students[i].preferences.clone());
                let regret = sol.value(v_regrets[i]) as u32;
                let s = students[i].preferences[(regret - 1) as usize];
                students[i].preferences.remove((regret - 1) as usize);
                students[i].assigned.insert(day.clone(), (s, regret));
                groups[s as usize]
                    .students
                    .entry(day.clone())
                    .or_insert(Vec::new())
                    .push(students[i].id);
            }
            return Some(Setup {
                students,
                groups,
                average_regret: 0.,
            });
        }
    };
}
