use std::collections::HashMap;

#[derive(Debug, Clone)]
pub struct Student {
    pub id: u32,
    pub name: String,
    pub email: String,
    pub preferences: Vec<u32>,
    pub assigned: HashMap<String, (u32, u32)>, // contains the Group with the regret
}

#[derive(Debug, Clone)]
pub struct Group {
    pub id: u32,
    pub name: String,
    pub short_name: String,
    pub students: HashMap<String, Vec<u32>>,
    pub average_regret: HashMap<String, f64>,
}

#[derive(Debug, Clone)]
pub struct Setup {
    pub students: Vec<Student>,
    pub groups: Vec<Group>,
    pub average_regret: f64,
}
